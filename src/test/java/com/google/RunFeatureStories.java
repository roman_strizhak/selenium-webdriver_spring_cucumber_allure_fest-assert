package com.google;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty", "json:target/cucumber.json", "html:target/cucumber.html" }, features = {
        "src/test/resources/stories" })
public class RunFeatureStories {

}

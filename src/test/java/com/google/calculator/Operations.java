package com.google.calculator;

public enum Operations {

    PLUS("+"),
    MINUS("−"),
    MULTIPLY("×"),
    DIVIDE("÷"),
    EQUAL("=");

    private final String operationSign;

    Operations(final String operationSign) {
        this.operationSign = operationSign;
    }

    @Override
    public String toString() {
        return operationSign;
    }
}
package com.google.utils;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration("classpath:spring.xml")
public class WebDriverUtil {

    static {
        DriversPathUtil.setDriversPath();
    }

    @Autowired
    private WebDriver webDriver;
    private String projectUrl;

    @PostConstruct
    private void setUp() {
        manageWebDriver();
        WaitUtil.setWebDriverWaitObject(getWebDriver());
        setProjectUrl(System.getProperty("project.url"));
    }

    protected String getProjectUrl() {
        return projectUrl;
    }

    private void setProjectUrl(final String systemProperty) {
        projectUrl = Optional.ofNullable(systemProperty)
                .orElseThrow(() -> new AssertionError("ERROR: project url wasn't defined in maven command line."));
    }

    protected WebDriver getWebDriver() {
        return webDriver;
    }

    protected WebElement $(final By by) {
        return getWebDriver().findElement(by);
    }

    private void manageWebDriver() {
        getWebDriver().manage().window().maximize();
        final long implicitlyWaitTimeout = 20;
        getWebDriver().manage().timeouts().implicitlyWait(implicitlyWaitTimeout, TimeUnit.SECONDS);
    }
}
package com.google.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtil {

    private static Wait<WebDriver> wait;

    public static void setWebDriverWaitObject(final WebDriver webDriver) {
        wait = new WebDriverWait(webDriver, 30, 5)
                .ignoring(StaleElementReferenceException.class, ElementNotVisibleException.class)
                .withMessage("Element wasn't found");
    }

    public static void waitUntilVisible(final By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
}
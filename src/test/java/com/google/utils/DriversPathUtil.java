package com.google.utils;

public final class DriversPathUtil {

    private static final String OS_ARCH = "os.arch";
    private static final String X64_ARCH = "amd64";
    private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";

    private DriversPathUtil() {
    }

    public static void setDriversPath() {
        switch (OperatingSystemUtil.getOperatingSystemType()) {
        case WINDOWS:
            setChromeDriverWindows();
            break;
        case MACOS:
            setChromeDriverOsx();
            break;
        case LINUX:
            setChromeDriverLinux();
            break;
        default:
            throw new IllegalArgumentException("Can't define Operating System");
        }
    }

    private static void setChromeDriverLinux() {
        if (X64_ARCH.equals(System.getProperty(OS_ARCH))) {
            System.setProperty(WEBDRIVER_CHROME_DRIVER, "drivers/linux/64bit/chromedriver");
        } else {
            System.setProperty(WEBDRIVER_CHROME_DRIVER, "drivers/linux/32bit/chromedriver");
        }
    }

    private static void setChromeDriverWindows() {
        System.setProperty(WEBDRIVER_CHROME_DRIVER, "drivers/windows/chromedriver.exe");
    }

    private static void setChromeDriverOsx() {
        System.setProperty(WEBDRIVER_CHROME_DRIVER, "drivers/osx/chromedriver");
    }
}
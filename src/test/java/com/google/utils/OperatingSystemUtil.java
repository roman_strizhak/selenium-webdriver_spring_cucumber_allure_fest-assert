package com.google.utils;

import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public final class OperatingSystemUtil {

    private static AtomicReference<OSType> detectedOS = new AtomicReference<>(null);

    private OperatingSystemUtil() {
    }

    public enum OSType {
        WINDOWS,
        MACOS,
        LINUX,
        OTHER
    }

    public static OSType getOperatingSystemType() {
        if (Objects.isNull(detectedOS.get())) {
            final String operationSystem = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
            final OSType expectValue = null;
            if (operationSystem.contains("mac") || operationSystem.contains("darwin")) {
                detectedOS.compareAndSet(expectValue, OSType.MACOS);
            } else if (operationSystem.contains("win")) {
                detectedOS.compareAndSet(expectValue, OSType.WINDOWS);
            } else if (operationSystem.contains("nux")) {
                detectedOS.compareAndSet(expectValue, OSType.LINUX);
            } else {
                detectedOS.compareAndSet(expectValue, OSType.OTHER);
            }
        }
        return detectedOS.get();
    }
}
package com.google.steps;

import com.google.pages.CalculatorPage;
import com.google.pages.SearchPage;

import com.google.utils.WebDriverUtil;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.fest.assertions.Assertions.assertThat;

public class CalculatorSteps extends WebDriverUtil {

    @Autowired
    private SearchPage searchPage;

    @Autowired
    private CalculatorPage calculatorPage;

    @Given("^a google search page$")
    public void googleSearchPage() {
        searchPage.open();
    }

    @Given("^a calculator application$")
    public void openCalculatorApplication() {
        searchPage.openCalculator();
    }

    @When("^I add (\\d+) and (\\d+)$")
    public void addNumbers(int a, int b) {
        calculatorPage.add(a, b);
    }

    @Then("^the result of the addition equals (\\d+)$")
    public void additionResult(String result) {
        assertThat(calculatorPage.getCalculationResult())
                .as("Calculation result").isEqualTo(result);
    }

    @When("^from (\\d+) I subtract (\\d+)$")
    public void subtractNumbers(int a, int b) {
        calculatorPage.subtract(a, b);
    }

    @Then("^the result of the subtraction equals (\\d+)$")
    public void subtractionResult(String result) {
        assertThat(calculatorPage.getCalculationResult())
                .as("Calculation result").isEqualTo(result);
    }

    @When("^I multiply (\\d+) and (\\d+)$")
    public void multiplyNumbers(int a, int b) {
        calculatorPage.multiply(a, b);
    }

    @Then("^the result of the multiplication equals (\\d+)$")
    public void multiplicationResult(String result) {
        assertThat(calculatorPage.getCalculationResult())
                .as("Calculation result").isEqualTo(result);
    }

    @When("^I divide (\\d+) on (\\d+)$")
    public void divideNumbers(int a, int b) {
        calculatorPage.divide(a, b);
    }

    @Then("^the result of the division equals (\\d+)$")
    public void divisionResult(String result) {
        assertThat(calculatorPage.getCalculationResult())
                .as("Calculation result").isEqualTo(result);
    }
}
package com.google.steps;

import com.google.pages.CalculatorPage;
import com.google.pages.SearchPage;
import com.google.utils.WebDriverUtil;

import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.fest.assertions.Assertions.assertThat;

public class SetOfNumbersSteps extends WebDriverUtil {

    @Autowired
    private SearchPage searchPage;
    @Autowired
    private CalculatorPage calculatorPage;

    private int a, b;

    @Given("^first number (\\d+)$")
    public void firstNumber(int firstNum) throws Throwable {
        a = firstNum;
    }

    @And("^second number (\\d+)$")
    public void secondNumber(int secondNum) throws Throwable {
        b = secondNum;
    }

    @When("^I multiply them in the calculator$")
    public void multiply() throws Throwable {
        searchPage.openCalculator();
        calculatorPage.multiply(a, b);
    }

    @Then("^the result equals (\\d+)$")
    public void result(String result) throws Throwable {
        assertThat(calculatorPage.getCalculationResult())
                .as("Calculation result").isEqualTo(result);
    }
}

package com.google.pages;

import com.google.utils.WebDriverUtil;

import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import static com.google.utils.WaitUtil.waitUntilVisible;

@Component
public class SearchPage extends WebDriverUtil {

    public void open() {
        getWebDriver().get(getProjectUrl());
    }

    public void openCalculator() {
        getWebDriver().navigate().to(getProjectUrl() + "search?q=1+-+1");
        waitUntilVisible(By.cssSelector("span[id='cwos']"));
    }
}

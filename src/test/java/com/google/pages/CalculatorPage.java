package com.google.pages;

import com.google.calculator.Operations;
import com.google.utils.WebDriverUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

@Component
public class CalculatorPage extends WebDriverUtil {

    private WebElement getCalcButton(String button) {
        return $(By.xpath(String.format("//span[text()='%s']", button)));
    }

    private void executeOperation(int firstNum, String mathOperation, int secondNum) {
        getCalcButton(String.valueOf(firstNum)).click();
        getCalcButton(mathOperation).click();
        getCalcButton(String.valueOf(secondNum)).click();
    }

    public String getCalculationResult() {
        getCalcButton(Operations.EQUAL.toString()).click();
        WebElement calculationResultField = $(By.cssSelector("span[id='cwos']"));
        return calculationResultField.getText();
    }

    public void add(int firstNum, int secondNum) {
        executeOperation(firstNum, Operations.PLUS.toString(), secondNum);
    }

    public void subtract(int minuend, int subtrahend) {
        executeOperation(minuend, Operations.MINUS.toString(), subtrahend);
    }

    public void multiply(int firstNum, int secondNum) {
        executeOperation(firstNum, Operations.MULTIPLY.toString(), secondNum);
    }

    public void divide(int dividend, int divider) {
        executeOperation(dividend, Operations.DIVIDE.toString(), divider);
    }

}

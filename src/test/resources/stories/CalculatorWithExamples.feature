Feature: Calculator with examples

  Background:
    Given a google search page

  @high
  Scenario Outline: Multiplication a set of the numbers
    Given first number <first>
    And second number <second>
    When I multiply them in the calculator
    Then the result equals <result>

    Examples:
      | first | second | result |
      | 2     | 3      | 6      |
      | 3     | 4      | 12     |
      | 4     | 5      | 20     |
      | 5     | 6      | 30     |
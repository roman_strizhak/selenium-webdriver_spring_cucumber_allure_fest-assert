Feature: Simple calculator operations

  Background:
    Given a google search page

  @low
  Scenario: Addition
    Given a calculator application
    When I add 2 and 3
    Then the result of the addition equals 5

  @low
  Scenario: Subtraction
    Given a calculator application
    When from 9 I subtract 6
    Then the result of the subtraction equals 3

  @high
  Scenario: Multiplication
    Given a calculator application
    When I multiply 5 and 5
    Then the result of the multiplication equals 25

  @high
  Scenario: Division
    Given a calculator application
    When I divide 9 on 3
    Then the result of the division equals 3
# TEST AUTOMATION FRAMEWORK

### About this project
TAF based on Selenium WebDriver, Spring, Cucumber, Fest-Assert, Allure frameworks.

### Required software
##### To install TAF the following software is required:
* [Java JDK 1.8 (64-bit version)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Apache Maven 3.3.9 or above](https://maven.apache.org/download.cgi)
* [Git](https://git-scm.com/downloads)
* [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download/download-thanks.html?code=IIC)

###### Useful Plugins for IntelliJ IDEA:
- CheckStyle-IDEA
- Eclipse Code Formatter
- Gerkin Syntax Support
- Pipe Table Formatter

#### Fast TAF check
```
$ git clone git@bitbucket.org:roman_strizhak/selenium-webdriver_spring_cucumber_allure_fest-assert.git
$ mvn clean generate-resources test -Dproject.url="https://www.google.com/" -Dcucumber.options="--tags @low" site
```

#### Download chrome drivers
```
$ mvn generate-resources
```

#### Run tests and generate report
```
$ mvn test -Dproject.url="https://www.google.com/" site
```
- Allure report will be generated at target/site/allure-maven-plugin/index.html

#### Run tests by tags
```
$ mvn test -Dproject.url="https://www.google.com/" -Dcucumber.options="--tags @high"
```